/*
 * File: jukebox-server.c
 *
 * Author: Lily Hoffman
 *
 *
 */

#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>


#define BACKLOG (10)
#define MAX_CLIENTS (1024)


//-----------PROTOCOL-----------//
/*|-------------------|
 *| Request message   |
 *|  int socket       |
 *|  int cmd          |
 *|  int param        |
 *|-------------------|
 * cmd {1: List, 2: Info, 3: Play}
 * param List(0), Info(ID), Play(ID)
 *
 *|-------------------|
 *| Response message  |
 *|  char* payload    |
 *|-------------------|
 *payload {list of songs, info of song(id), song(id)}
 */
//-----------PROTOCOL-----------//

struct ClientData{
    int socket;
    
    int cmd;
    int param;
    int* client_buffer;
    
    int ready_for_writing;
    int ready_for_reading;
    int bytes_recvd;
};typedef struct ClientData ClientData;

struct ServerData{
    char* payload;
};typedef struct ServerData ServerData;

struct Song{
    int id;
    char* songname; //ascii string (contains .mp3)
    char* infostring; //ascii string
    char* songmp3; //byte string
    int mp3size;
    int total_num_songs;
};typedef struct Song Song;

// forward declarations
int acceptConnection(int server_socket);
void setNonBlocking(int sock);
int filter(const struct dirent *ent);
Song* readMP3Files(char *dir);
ServerData* interpretResponse(int cmd, int param, Song* SongLibrary);
ServerData* giveList(Song* SongLibrary);
ServerData* giveInfo(Song* SongLibrary, int id);
ServerData* giveSong(Song* SongLibrary, int id);
void printLibrary(Song* SongLibrary);


int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Usage:\n%s <port> <filedir>\n", argv[0]);
        exit(0);
    }
    // Get the port number from the arguments.
    uint16_t port = (uint16_t) atoi(argv[1]);
    
    //Initialize & set up server socket
    int serv_sock = socketInit(port);
    
    //Select uses sets of file descriptors (type: fd_set) to determine which file descriptors it will monitor.
    fd_set all_fds, read_fds, write_fds;
    FD_ZERO(&all_fds);
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);
    
    //Add server socket to list of all socket file descriptor. Will check if available for reading (have new client trying to connect)
    FD_SET(serv_sock, &all_fds);
    int max_fd = serv_sock;
    
    
    //Initialize SongLibrary
    Song* SongLibrary = readMP3Files(argv[2]);
    printf("Waiting for connection...\n");
    
    //Initialize data storage for EACH client
    int i;
    int scalar = 10;
    ServerData* server_response = malloc(scalar*max_fd*sizeof(ServerData));
    ClientData* client_request = malloc(scalar*max_fd*sizeof(ClientData));
    
    //Allocate buffer to store request from client
    int recv_buff_size = 2*sizeof(int);
    for(i = 0; i <scalar*max_fd; i++){
        client_request[i].socket = i;
        client_request[i].client_buffer = malloc(recv_buff_size);
        
        client_request[i].ready_for_writing = 0;
        client_request[i].ready_for_reading = 1;
    }
    
    
    while (1) {
        //Look for reads or writes on all sockets.
        read_fds = all_fds;
        write_fds = all_fds; //ready for wriitng = buffer empty for storage
        
        // select() will block until one of the sockets asked about is ready for recving/sending.
        val = select(max_fd+1, &read_fds, &write_fds, NULL, NULL);
        if (val < 1) {
            perror("select");
            continue;
        }
        
        
        // Check the FD_SETs after select returns to see which sockets are ready for recv/send.
        for (i = 0; i <= max_fd; ++i) {
            // If there are sockets ready for reading, read (cmd, param)
            if (FD_ISSET(i, &read_fds)) {
                if (i == serv_sock) {
                    // If server socket is ready for "reading" we have a new client wanting to connect so accept it.
                    int client_fd = acceptConnection(serv_sock);
                    FD_SET(client_fd, &all_fds);
                    if (client_fd > max_fd) max_fd = client_fd;
                    printf("\n\n-----------------------------------\n");
                    printf("Accepted a new connection!\n");
                    printf("-----------------------------------\n");
                    setNonBlocking(client_fd);
                    client_request[client_fd].ready_for_reading = 1;
                }
                // This wasn't the server socket. New client has sent us data. recv it
                else {
                    if(client_request[i].ready_for_reading == 1){
                        printf("\n\n\nRECV data from client (%d): \n", i);
                        
                        int total_bytes_recvd = 0;
                        while(total_bytes_recvd < recv_buff_size){
                            int bytes_recvd = recv(i, client_request[i].client_buffer, recv_buff_size, 0);
                            if(bytes_recvd == -1){
                                perror("recv\n");
                            }
                            total_bytes_recvd += bytes_recvd;
                        }
                        client_request[i].bytes_recvd = total_bytes_recvd;
                        
                        client_request[i].cmd = (ntohl((uint32_t)client_request[i].client_buffer[0]))/256;
                        client_request[i].param = (ntohl((uint32_t)client_request[i].client_buffer[1]))/256;
                        
                        client_request[i].ready_for_writing = 1; //command received, ready to write response to client
                        client_request[i].ready_for_reading = 0;
                    }
                }
            }
            if (FD_ISSET(i, &write_fds)){
                if(client_request[i].ready_for_writing == 1 ){
                    printf("...sending data to client: \n");
                    int bytes_sent;
                    //Play music
                    if(client_request[i].cmd == 3){
                        server_response[i]= *interpretResponse(client_request[i].cmd, client_request[i].param, SongLibrary);
                        bytes_sent = send(client_request[i].socket, (server_response[i].payload), SongLibrary[client_request[i].param].mp3size, 0);
                    }
                    else{
                        server_response[i]= *interpretResponse(client_request[i].cmd, client_request[i].param, SongLibrary);
                        bytes_sent = send(client_request[i].socket, (server_response[i].payload), strlen((server_response[i].payload)), 0);
                    }
                    if (bytes_sent > 0){
                        perror("send");
                    }
                    printf("%s\n", (server_response[i].payload));
                    printf("bytes_sent = %d\n", bytes_sent);
                    
                    client_request[i].ready_for_writing = 0; //response written
                }
            }
        }
    }
}

/*
 * Creates a new socket and starts listening on that socket for new
 * connections.
 *
 *@param port The port number on which to listen for connections.
 *@returns The socket file descriptor
 */
int socketInit(uint16_t port){
    // Create the socket to listen on.
    int serv_sock = socket(AF_INET, SOCK_STREAM, 0);
    int val = setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if (val < 0) {
        perror("setsockopt");
        exit(1);
    }
    setNonBlocking(serv_sock);
    
    // Bind socket and start listening for connections.
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    
    // Bind socket and start listening for connections.
    val = bind(serv_sock, (struct sockaddr*)&addr, sizeof(addr));
    if(val < 0) {
        perror("bind");
        exit(1);
    }
    
    // Set socket to listen for connections (to later accept)
    val = listen(serv_sock, BACKLOG);
    if(val < 0) {
        perror("listen");
        exit(1);
    }
    
    //return the server socket
    return serv_sock
    
}

void printLibrary(Song* SongLibrary){
    printf("SongLibrary:\n");
    int s = 0;
    //if(SongLibrary[0] == null) return;
    for(s = 0; s < SongLibrary[0].total_num_songs; s++){
        Song song = SongLibrary[s];
        song.id = SongLibrary[s].id;
        song.songname = SongLibrary[s].songname;
        song.infostring = SongLibrary[s].infostring;
        song.songmp3 = SongLibrary[s].songmp3;
        printf("-------------------------------------------------\n");
        printf("(%d) %s\n %s\n mp3: %s \n\n", song.id, song.songname, song.infostring, song.songmp3);
        printf("-------------------------------------------------\n");
    }
}

ServerData* interpretResponse(int cmd, int param, Song* SongLibrary){
    printf("Interpreting... cmd: %d param: %d\n", cmd, param);
    ServerData* response = NULL;
    if (cmd >= 256)
        cmd /=256;
    if(cmd == 1)
        response = giveList(SongLibrary);
    if(cmd == 2)
        response = giveInfo(SongLibrary, param);
    if(cmd == 3)
        response = giveSong(SongLibrary, param);
    return response;
}
/*
 * Creates payload in the form
 * 1 song1\n
 * 2 song2\n
 * 3 song3\n
 * etc.
 *
 * Places list in payload buffer and encapsulates buffer in ResponseMessage
 */
ServerData* giveList(Song* SongLibrary){
    printf("Give list\n");
    int num_songs = SongLibrary[0].total_num_songs;
    ServerData* list_response = malloc(sizeof(ServerData));
    
    //Determine length of char* buffer
    int i;
    int songs_length = 0;
    for(i = 0; i < num_songs; i++){
        songs_length += (strlen(SongLibrary[i].songname) + 3); //+3 for null terminator, then for space & new line that we add
    }
    int max_digits = 0;
    while(num_songs>0){
        num_songs/=10;
        max_digits++;
    }
    int payload_length = songs_length*sizeof(char) + num_songs*max_digits*sizeof(char);
    char* payload = malloc(payload_length);
    memset(payload, 0, payload_length);
    
    num_songs = SongLibrary[0].total_num_songs;
    for(i = 0; i < num_songs; i++){
        //convert (int id) to char* representation
        int id = SongLibrary[i].id;
        int num_digits_in_id = 0;
        while(id>0){
            id/=10;
            num_digits_in_id++;
        }
        char* id_char_buffer = malloc(num_digits_in_id*sizeof(char));
        sprintf(id_char_buffer, "%d", SongLibrary[i].id);
        
        char* name = malloc(strlen(SongLibrary[i].songname));
        name = SongLibrary[i].songname;
        char* newline = "\n";
        char* space = " ";
        
        //Add list to payload
        strcat(payload, id_char_buffer);
        strcat(payload, space);
        strcat(payload, name);
        strcat(payload, newline);
    }
    
    //clear out unused buffer
    //memset(payload + current_index, 0, payload_length - current_index);
    
    //list_response->status = 1;
    list_response->payload = payload;
    return list_response;
}

ServerData* giveInfo(Song* SongLibrary, int id){
    printf("GiveInfo\n");
    int num_songs = SongLibrary[0].total_num_songs;
    ServerData* info_response = malloc(sizeof(ServerData));
    if(id >= num_songs){
        //info_response->status = -1;
        info_response->payload = "Song not available";
    }
    else{
        char* song_info = SongLibrary[id].infostring;
        //info_response->status = 1;
        info_response->payload = song_info;
    }
    return info_response;
    
}

ServerData* giveSong(Song* SongLibrary, int id){
    printf("GiveSong\n");
    int num_songs = SongLibrary[0].total_num_songs;
    ServerData* song_response = malloc(sizeof(ServerData));
    if(id > num_songs){
        //song_response->status = -1;
        song_response->payload = "Song not available";
    }
    else{
        char* mp3 = SongLibrary[id].songmp3;
        //song_response->status = 1;
        song_response->payload = mp3;
    }
    return song_response;
}


/**
 * Accepts a connection and returns the socket descriptor of the new client
 * that has connected to us.
 *
 * @param server_socket Socket descriptor of the server (that is listening)
 * @return Socket descriptor for newly connected client.
 */
int acceptConnection(int server_socket) {
    struct sockaddr_storage their_addr;
    socklen_t addr_size = sizeof(their_addr);
    int new_fd = accept(server_socket, (struct sockaddr *)&their_addr,
                        &addr_size);
    if (new_fd < 0) {
        perror("accept");
        exit(1);
    }
    
    return new_fd;
}

/*
 * Use fcntl (file control) to set the given socket to non-blocking mode.
 *
 * @info Setting your sockets to non-blocking mode is not required, but it
 * might help with your debugging.  By setting each socket you get from
 * accept() to non-blocking, you can be sure that normally blocking calls like
 * send, recv, and accept will instead return an error condition and set errno
 * to EWOULDBLOCK/EAGAIN.  I would recommend that you set your sockets for
 * non-blocking and then explicitly check each call to send, recv, and accept
 * for this errno.  If you see it happening, you know that you're attempting
 * to call one of those functions when you shouldn't be.
 *
 * @param sock The file descriptor for the socket you want to make
 * 				non-blocking.
 */
void setNonBlocking(int sock) {
    /* Get the current flags. We want to add O_NONBLOCK to this set. */
    int socket_flags = fcntl(sock, F_GETFD);
    if (socket_flags < 0) {
        perror("fcntl");
        exit(1);
    }
    
    /* Add in the O_NONBLOCK flag by bitwise ORing it to the old flags. */
    socket_flags = socket_flags | O_NONBLOCK;
    
    /* Set the new flags, including O_NONBLOCK. */
    int result = fcntl(sock, F_SETFD, socket_flags);
    if (result < 0) {
        perror("fcntl");
        exit(1);
    }
    
    /* The socket is now in non-blocking mode. */
}

/*
 * Checks wheter a filename ends in '.mp3'.
 * @info This is used below in readMP3Files. You probably don't need to use
 * it anywhere else.
 * @param ent Directory entity that we are going to check.
 * @return 0 if ent doesn't end in .mp3, 1 if it does.
 */
int filter(const struct dirent *ent) {
    int len = strlen(ent->d_name);
    return !strncasecmp(ent->d_name + len - 4, ".mp3", 4);
}

/*
 * Given a path to a directory, this function scans that directory (using the
 * handy scandir library function) to produce an alphabetized list of files
 * whose names end in ".mp3".  For each one, it then also checks for a
 * corresponding ".info" file and reads that in its entirety.
 *
 *
 * @param dir String that represents the path to the directory that you want
 * 				to check.
 *
 * Creates list of "Song" structs from directory contents
 *
 * @return Array of "Song" structs {id, songname, infostring, mp3string}
 */
Song* readMP3Files(char *dir) {
    struct dirent **namelist;
    int i;
    
    /*Read directory dir and build array of pointers to directory entries ending in .mp3 (via malloc)
     *Pointer to array of directories is store in namelist
     *Returns number of entries in the array
     */
    int num_files = scandir(dir, &namelist, filter, alphasort);
    if (num_files < 0) {
        perror("scandir");
        exit(1);
    }
    
    //Initialize song library
    Song* SongLibrary = malloc(num_files*sizeof(Song));
    
    char* null = malloc(sizeof(char));
    null = "\0";
    /*Look through all mp3 files in directory*/
    for (i = 0; i < num_files; ++i) {
        int bytes_read = 0;
        int total_read = 0;
        char path[1024];
        char songpath[1024];
        
        FILE *mp3_file = NULL;
        char *mp3_byte_string = NULL;
        
        FILE *infofile = NULL;
        char *infostring = NULL;
        
        /* namelist[i]->d_name now contains the name of an mp3 file. */
        printf("(%d) %s\n", i, namelist[i]->d_name);
        int song_name_len = strlen(namelist[i]->d_name)+1; //+1 for null terminator
        char* song_name = malloc(song_name_len-4); //-4 for [.mp3]
        memset(song_name, 0, song_name_len-4);
        strncpy(song_name, namelist[i]->d_name, song_name_len-5); //copy everything up to the .mp3\0
        strcat(song_name, null); //add null terminator after .mp3\0 truncated
        
        /* Build a path to a possible input file. */
        strcpy(path, dir);
        strcat(path, "/");
        strcat(path, namelist[i]->d_name);
        strcat(path, ".info");
        
        
        strcpy(songpath, dir);
        strcat(songpath, "/");
        strcat(songpath, namelist[i]->d_name);
        
        //READ MP3 FILE
        int songsize = 1024;
        mp3_file = fopen(songpath, "rb");
        if(mp3_file == NULL){
            perror("mp3_file\n");
        }
        else{
            /* Found and opened the mp3 file. */
            mp3_byte_string = malloc(songsize);
            
            do{
                songsize *=2;
                mp3_byte_string = realloc(mp3_byte_string, songsize);
                bytes_read = fread(mp3_byte_string + total_read, 1, songsize-total_read, mp3_file);
                total_read += bytes_read;
            }while(bytes_read > 0);
            fclose(mp3_file);
            memset(mp3_byte_string + total_read, 0,songsize - total_read);
        }
        
        
        //READ INFO FILE
        bytes_read = 0;
        total_read = 0;
        infofile = fopen(path, "r");
        if (infofile == NULL) {
            infostring = "No information available.";
        } else {
            /* We found and opened the info file. */
            int infosize = 1024;
            infostring = malloc(infosize);
            
            do {
                infosize *= 2;
                infostring = realloc(infostring, infosize);
                
                //Reads (infosize - total_read) objects, each 1 byte long from "infofile"
                //Stores these obj in "infostring + total_read"
                bytes_read = fread(infostring + total_read, 1, infosize - total_read, infofile);
                total_read += bytes_read;
            } while (bytes_read > 0);
            
            fclose(infofile);
            
            /* Zero-out the unused space at the end of the buffer. */
            memset(infostring + total_read, 0, infosize - total_read);
        }
        
        /* infostring now contains the info data for this song. */
        /* FIXME: Use these info strings when clients send info commands. */
        //printf("Info:%s\n\n", infostring);
        
        //Fill SongLibrary
        Song song;
        song.id = i;
        song.songname = song_name;//will contain .mp3
        song.infostring = infostring;
        song.songmp3 = mp3_byte_string;
        song.mp3size = songsize;
        song.total_num_songs = num_files;
        SongLibrary[i] = song;        
        free(namelist[i]);
    }
    //printLibrary(SongLibrary);
    printf("\n\n");
    free(namelist);
    
    /* Return the number of files we found. */
    return SongLibrary;
}
