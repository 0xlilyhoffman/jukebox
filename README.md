# Internet Jukebox

This project enables a user to obtain a list of songs on a server, and stream requested songs over the internet.

I implemented an interactive client program in Java and a server program in C to build an internet Jukebox. I also designed a protocol for communication between the client and server. 

The server is written in C and uses select() to handle synchronous IO multiplexing.

The client program is interactive and offers the following requests to be made to the server:

 1. list
 2. info [song number]
 3. play [song number]
 4. stop
 
 
 The server handles the commands as follows: 
 
 * list: *Retrieves a list of songs that are available on the server, along with their ID numbers.*
 * info [song number]: *Retrieves information from the server about the song with the specified ID number.*
 * play [song number]: *Begins playing the song with the specified ID number*
 * stop: *Stops playing the current song*
 