package edu.sandiego.comp375.jukebox;
import java.io.*;
import java.net.Socket;
import java.util.*;
import java.lang.*;

public class AudioClient {
	public static void main(String[] args) throws Exception {
        Scanner keyboard = new Scanner(System.in);
        boolean playing = false;
        Thread player_thread = null;
        Socket socket = null;
    
        while(true){
            displayMenu();
            int choice = keyboard.nextInt();
            switch(choice){
                case 1: handleList();
                        break;
                case 2: handleInfo();
                        break;
                case 3: if (socket != null) socket.close();
                        socket = handlePlay(player_thread, socket);
                        break;
                case 4: if (socket != null) socket.close();
                        break;
                case 5: System.exit(0);
                default: System.out.println("Invalid command\n"); 
            }
        }
    } 


    public static void displayMenu(){
        System.out.println("Enter a number from the options below...\n");
        System.out.println("\t(1)List\n\t(2)Info [song number] \n\t(3)Play [song number]\n\t(4)Stop\n\t(5)Exit");
    }
    
    public static void handleList(){
        int cmd = 1;
        int param = 0;
        try{
            Socket socket = new Socket("localhost", 8080);
            if(socket.isConnected()){
                System.out.println("socket connected\n");
                try{
                    //set up
                    DataOutputStream socket_output = new DataOutputStream(socket.getOutputStream());
        
                    //send list request to server (1, 0)
                    socket_output.writeInt(cmd);
                    socket_output.writeInt(param);
                    System.out.println("sent [1] [0] \n");

                    //receive response from server
                    BufferedInputStream socket_input = new BufferedInputStream(socket.getInputStream(), 2048);
                    System.out.println("Available: " + socket_input.available());
                    while(socket_input.available() >0){
                        char c = (char)socket_input.read(); //recv from server
                        System.out.print(c); //print recieved message
                    }
                    socket_output.close();
                    socket_input.close();

                }catch(IOException e){
                    System.out.println(e);
                    System.exit(1);
                }
            socket.close();

            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    

    public static void handleInfo(){
        int cmd = 2;
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter song number\n");
        int song_number  = keyboard.nextInt();
        try{
            Socket socket = new Socket("localhost", 8080);
            if(socket.isConnected()){
                try{
                    //Init socket output
                    DataOutputStream socket_output = new DataOutputStream(socket.getOutputStream());

                    //send request to server
                    int param = song_number;
                    socket_output.writeInt(cmd);
                    socket_output.writeInt(param);
                    System.out.println("sent [2] [" + param + "]");

                    //init socket input
                    BufferedInputStream socket_input = new BufferedInputStream(socket.getInputStream(), 2048);   
                    System.out.println("Available: " + socket_input.available());
                    while(socket_input.available() > 0){
                        char c = (char)socket_input.read(); //recv from server
                        System.out.print(c); //print recieved message
                    }
                socket_output.close();
                socket_input.close();
                }catch(IOException e){
                    System.out.println(e);
                    System.out.println("Read failed");
                    System.exit(1);
                }
            }
            socket.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
        
    public static Socket handlePlay(Thread player_thread, Socket socket){
       int cmd = 3;
       Scanner keyboard = new Scanner(System.in);
       System.out.println("Enter song number to play\n");
       int song_number  = keyboard.nextInt();    
       try{
            socket = new Socket("localhost", 8080);
            if(socket.isConnected()){
                //set up
                DataOutputStream socket_output = new DataOutputStream(socket.getOutputStream());
                //send command to server
                int param = song_number;
                socket_output.writeInt(cmd);
                socket_output.writeInt(param);
                System.out.println("Sent [3] [" +  param + "]");

                BufferedInputStream socket_input = new BufferedInputStream(socket.getInputStream(), 2048);   
                player_thread = new Thread(new AudioPlayerThread(socket_input));
                player_thread.start();
                System.out.println("Started new thread\n");
                //socket_output.close();
                //socket_input.close();
            }
        //socket.close();
        }catch(IOException e){
            System.out.print(e);
            System.err.println("Couldn't get I/O for the connection to host");
        }catch(Exception e){
            System.out.println(e);
        }
        return socket;
    }

    public static boolean handleStop(boolean playing, Thread player_thread){
        if(playing){ 
            player_thread.stop();
            player_thread.interrupt();
            playing = false;
        }
        return playing;
    }

    public static void handleExit(Thread player_thread){
        System.out.println("Exiting...\n");
        player_thread.interrupt();
        System.exit(0);
    }
}   
